# weather-cli

Command line client for fetching weather data from the [Open Weather API](https://openweathermap.org).

This small client uses the OpenWeather API to provide forecasts based on current location, zip code, exact 
coordinates, and city name. In addition, the client is capable of connecting to its partner project, 
[SimplrWeather](https://github.com/vincentmoreschi/SimplrWeather), which provides a gRPC backend for retrieving
historical weather data.

### Instructions

A file named `.env` must be placed in the root of the project or in the directory from which you will run the compiled
binary. You *must* provide a port number and an OpenWeather API key in this file:

```
API_KEY=xxxxxxxxxxxxxxxx
PORT=xxxxx
```

Once complete, you can start the client by runnning `go run cmd/*.go` from the root of the project. Optionally, binary
files can also be compiled with `go build`.

By default, the client will automatically obtain your approximate location using your IP address. Additional data based
on IP address is fetched from [ipinfo.io](https://ipinfo.io). If you don't want your IP address sent to that API, simply
choose among the other options.

Basic options include:
- Automatically obtain forecast for your location
- Search by zip code
- Search by city name

Advanced options:
- Change units (default imperial)
- Search with exact location (latitude and longitude)
- Historical data

#### Screenshot

![Main menu](info/image.png)
